EYui Demo
=========

Instructions for demo.

1. download this demo source code from here (look at "get source" command bar) and uncompress into your selected destination.
example: 
		c:/code/eyuidemo.

2. download a recent copy of EYui. 
[download eyui as zip](https://bitbucket.org/christiansalazarh/eyuidemo/get/b892718ccd5f.zip "download eyui as zip")

3. uncompress the eyui zip file (step 2) into:

		c:/code/eyuidemo/protected/extensions/eyui
		
		(create the 'extensions' directory if needed)
		
4. your LS command (or dir command in windows) must show you something similar to:

		/C/code/eyuidemo/protected/extensions/eyui
		$ ls
		EYuiAction.php          EYuiFormGroup.php       EYuiWidget.php
		EYuiActionRunnable.php  EYuiFormIStorage.php    README.md
		EYuiFooWidget.php       EYuiFormIValidator.php  assets
		EYuiForm.php            EYuiFormModel.php       eyuiformdb-mysql-schema.sql
		EYuiFormDb.php          EYuiFormPage.php        eyuiformdb-sqlite-schema.sql
		EYuiFormField.php       EYuiSearch.php          eyuisearch.gif
		EYuiFormFieldDef.php    EYuiSearchable.php      uml
		
5. Please be aware of names sensibility on Linux:  "eyui" is not the same as "EYui".