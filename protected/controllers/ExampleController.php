<?php

class ExampleController extends Controller
{
	public function actions()
	{
		return array();
	}
	
	/**
		demo for EYuiSearch
	*/
	public function actionEYuiSearch() 
	{ 
		$model=new Example; 

		if(isset($_POST['Example'])) 
		{ 
			$model->attributes=$_POST['Example']; 
			if($model->validate()) 
			{ 
				$this->redirect(array('ok'));
				return; 
			} 
		} 
		$this->render('eyuisearch',array('model'=>$model)); 
	}

	public function actionOk(){
		$this->renderText("Ok example finished. <a href='index.php?r=example/eyuisearch'>Start Again</a>");
	}

	/**
		demo for EYuiForm
		
		it will use TblUser as the target user model for this form.
	*/
	public function actionEYuiForm(){
		$user = TblUser::model()->findByPk(1);
		$this->render('eyuiform',array('user'=>$user));
	}
	public function actionEYuiForm2(){
		$user = TblUser::model()->findByPk(1);
		$this->render('eyuiform2',array('user'=>$user));
	}
	public function actionEYuiFormEditor(){
		$this->render('eyuiformeditor');
	}
	
	public function actionEYuiAjaxAction() 
	{ 
		$this->render('eyuiajaxaction'); 
	}	
	
	public function actionEYuiRelation() 
	{ 
		$model = TblCompany::model()->findByPk(10);
	
		$this->render('eyuirelation',array('model'=>$model)); 
	}	
	
	
	public function actionDataExport($userid=null) {
		
		// 'mymodelid' is referenced in: eyuidemo\protected\views\example\eyuiform2.php
		
		// a writeable directory for tmp files
		$tmp_folder = 'assets/';
		
		// 1. load the form structure (pages, groups and fields)
		$exporter = EYuiFormDataExport::newModel(
			EYuiFormEditorDb::model('mymodelid','form1'));

		// 2. build the required CSV files (empty files, only column definition)
		$exporter->prepareFiles($tmp_folder);
		
		if($userid == null){
			//	3.a) export records for each user
			foreach(TblUser::model()->findAll() as $user)
				$exporter->insertModel(EYuiFormDb::newModel($user),'form1');
		}
		else{
			//	3.b) export records for selected user
			$user = TblUser::model()->findByPk($userid);
			$exporter->insertModel(EYuiFormDb::newModel($user),'form1');
		}
		
		// AT THIS POINT YOU MUST HAVE CSV FILES STORED IN YOUR $tmp_folder
		// EACH FILE NAME IS THE FORM PAGE NAME.
		
		
		// 4. outputs all generated csv files into a single zip.
		
		$zipName = "exported-data.zip";
		$destZip = $tmp_folder.$zipName;
		$zip = new ZipArchive;
		if ($zip->open($destZip,ZIPARCHIVE::OVERWRITE) === TRUE){
			// exporter will return the generated filenames 
			foreach($exporter->getFiles() as $fname){
				$fname = utf8_decode($fname);
				$addfile = $tmp_folder.$fname;
				if(!$zip->addFile($addfile,$fname))
					throw new Exception("[error addZip: ".$addfile."]");
			}
			$zip->close();
			
			// 5. at this point you must have a zip file. now output it to browser for direct download
			//
			header("Cache-Control: public");
			header("Content-Description: File Transfer");
			header("Content-Transfer-Encoding: binary");
			header("Content-Disposition: attachment; filename=".$zipName);
			header("Content-Type: application/octet-stream");
			echo readfile($destZip);
		}
	}
}