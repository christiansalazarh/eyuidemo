<?php

/**
 * This is the model class for table "tbl_companyjob".
 *
 * The followings are the available columns in table 'tbl_companyjob':
 * @property integer $id
 * @property integer $job_id
 * @property integer $company_id
 */
class TblCompanyJob extends CActiveRecord
	implements EYuiRelationIRelation
{
	public function eyuirelation_insert($widgetid,$masterPrimaryId, $optionPrimaryId){
		// $masterPrimaryId is: company_id, the X part
		// $optionPrimaryId is: job_id, the Y part
		// and TblCompanyJob is : the XY relationship.
		//
		$inst = new TblCompanyJob;
		$inst->company_id = $masterPrimaryId;
		$inst->job_id = $optionPrimaryId;
		if($inst->insert()){
			return $inst->id;
		}
		else
		return null;
	}
	
	public function eyuirelation_remove($widgetid,$primaryId){
		$inst = self::findByPk($primaryId);
		if($inst != null)
			if($inst->delete())
				return true;
		return false;
	}
	
	/**
		must return a CHtml::listData
	*/
	public function eyuirelation_listData($widgetid,$masterPrimaryId){
		$models = self::model()->findAllByAttributes(array('company_id'=>$masterPrimaryId));
		$items = array();
		foreach($models as $model)
			$items[$model->id] = $model->job->name;
		return $items;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TblCompanyJob the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_companyjob';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('job_id, company_id', 'required'),
			array('job_id, company_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, job_id, company_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'job' => array(self::BELONGS_TO, 'TblJob', 'job_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'job_id' => 'Job',
			'company_id' => 'Company',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('job_id',$this->job_id);
		$criteria->compare('company_id',$this->company_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}