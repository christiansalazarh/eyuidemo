<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1>EYui Example</h1>

<p>Dear user, this is an example to test EYui Widgets</p>

<?php echo CHtml::link("Test EYuiSearch",array('example/eyuisearch'));?>
<br/>
<?php echo CHtml::link("Test EYuiForm",array('example/eyuiform'));?>
<br/>
<?php echo CHtml::link("Test EYuiForm (using editor)",array('example/eyuiform2'));?>
<br/>
<?php echo CHtml::link("Test EYuiFormEditor",array('example/eyuiformeditor'));?>
<br/>
<?php echo CHtml::link("Test EYuiAjaxAction",array('example/eyuiajaxaction'));?>
<br/>
<?php echo CHtml::link("Test EYuiRelation",array('example/eyuirelation'));?>


