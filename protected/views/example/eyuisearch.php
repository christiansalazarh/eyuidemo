<h1>EYuiSearch Tester</h1>

<?php
/* @var $this ExampleController */
/* @var $model Example */
/* @var $form CActiveForm */
?>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'example-create-form',
	'enableAjaxValidation'=>false,
)); ?>

	<div class="row" style='background-color: #eeeeff; padding: 5px;'>
		<p style='color: blue'><b>Search a user by part of its user name, next click 'Search' button:</b><br/>for demo, available usernames are:
			test1, test2, test3,...,test10
			<br/>
			try finding 'test' in textbox below:
		</p>
		<div style='width: 400px;'>
		<?php 
			$this->widget('ext.eyui.EYuiSearch'
				,array(
					'model'=>$model,
					'attribute'=>'userid',
					'searchModel'=>TblUser::model(),
					'attributes'=>array('id','userDescription'),
				)
			);
		?>
		</div>
	</div>
	
	
	<div class="row" style='background-color: #ffffee; padding: 5px;'>
		<p>After you search your selection will be shown here:</p>
		<?php echo $form->labelEx($model,'userid'); ?>
		<?php echo $form->textField($model,'userid'); ?>
		<?php echo $form->error($model,'userid'); ?>
	</div>
	

	<div class="row">
		<?php echo $form->labelEx($model,'message'); ?>
		<?php echo $form->textField($model,'message'); ?>
		<?php echo $form->error($model,'message'); ?>
	</div>


	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->