<h1>EYuiFormEditor Demo</h1>
<p>
<?php echo CHtml::link("view form",array('example/eyuiform2')) ?>
</p>

<?php 
	/*
		please ensure that 'mymodelid' and 'form1' are the same values applied to 	eyuidemo\protected\views\example\eyuiform2.php in: 
			'pages'=>EYuiFormEditorDb::model('mymodelid',"form1"),
	*/
$this->widget('ext.eyui.EYuiFormEditor',array(
	// please be sure 'mymodelid' and 'form1' are the same whenever you need to fill this form
	// as a final user.
	'model'=>EYuiFormEditorDb::model('mymodelid',"form1"),
));
?>