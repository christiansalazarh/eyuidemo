<h1>EYuiAjaxAction</h1>
<p>This widget invokes an Action via Ajax, please click the link below:</p>
	<?php 
		$this->widget('ext.eyui.EYuiAjaxAction'
			,array(
				'id'=>'myId',
				'action'=>array('site/Sample'),
				'label'=>'Test Me',
				'labelOn'=>'Sending...',
				'onBeforeAjaxCall'=>'function(){ $("#mylogger").html("please wait..."); }',
				'onSuccess'=>'function(data){ $("#mylogger").html(data); }',
				'onError'=>'function(e){ $("#mylogger").html(e.responseText); }',
				'htmlOptions'=>array('class'=>'yourclass'),
			)
		);
	?>
	
	<div id='mylogger'></div>