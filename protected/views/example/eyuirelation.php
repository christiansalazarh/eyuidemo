<h1>EYuiRelation</h1>
<p>EYuiRelation handles a relationship between models in a visual way. In this example, a company
will have Departments and Jobs.</p>
<p style='background-color: yellow;'>For demo pruposes the selected model will be a company with ID=10 stablished in actionEYuiRelation() </p>

	<?php 
		$this->widget('ext.eyui.EYuiRelation'
			,array(
				'debug'=>false,
				'id'=>'jobrelationship',
				'onError'=>'function(e){ $("#mylogger").html(e.responseText); }',
				'model'=>$model,
				'title'=>'Jobs:<hr/>',
				'optionsClassName'=>'TblJob',
				'relationClassName'=>'TblCompanyJob',
				'htmlOptions'=>array('style'=>'width: 300px; float: left; margin-right: 10px;'),
			)
		);
	?>	
	<?php 
		$this->widget('ext.eyui.EYuiRelation'
			,array(
				'debug'=>false,
				'id'=>'departmentrelationship',
				'onError'=>'function(e){ $("#mylogger").html(e.responseText); }',
				'model'=>$model,
				'title'=>'Departments:<hr/>',
				'optionsClassName'=>'TblDepartment',
				'relationClassName'=>'TblCompanyDepartment',
				'htmlOptions'=>array('style'=>'width: 300px; float: left;'),
			)
		);
	?>	
	
	<div id='mylogger'></div>